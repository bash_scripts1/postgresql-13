# postgresql-13

Перед установкой скачать и скопировать файл с нужным вам функционалом на ВМ в директорию /root/ после чего открыть и вписать в переменный в шапки файла свои переменные! 
postgresql_13_dump_allDBs_inRPM - запускать в папке /root . Сам сделает дамп, запакует в zip и сохранит в /root если postgresql установлен через rpm

postgresql_13_pgAdmin_inDocker - установит postgresql_13 и pgAdmin в контейнеры docker

postgresql_13_rpm_pgAdmin_inDocker - установит postgresql_13 как rpm, а pgAdmin в контейнеры docker
